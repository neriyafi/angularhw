// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAZj9HVXh3RSO2rT2xHa_CYU1SdYJMS_44",
  authDomain: "angularhome-c977e.firebaseapp.com",
  databaseURL: "https://angularhome-c977e.firebaseio.com",
  projectId: "angularhome-c977e",
  storageBucket: "angularhome-c977e.appspot.com",
  messagingSenderId: "1042684088564",
  appId: "1:1042684088564:web:0f3e5eaaa5e9d62361cea1",
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
