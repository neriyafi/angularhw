import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-addbook',
  templateUrl: './addbook.component.html',
  styleUrls: ['./addbook.component.css']
})
export class AddbookComponent implements OnInit {

  constructor(private booksservice:BooksService,
              private router:Router,private route:ActivatedRoute,
              public authService:AuthService) { }
  book_name:string;
  book_author:string; 
  id:string;
  isEdit:boolean = false;
  buttonText:string = "Add book"
  userId:string;

  onSubmit(){ 
    if(this.isEdit){
      this.booksservice.updateBook(this.userId ,this.id, this.book_name, this.book_author)
    } 
    else{
      this.booksservice.addBooks(this.userId,this.book_name,this.book_author)
    }
   
    this.router.navigate(['/books']);
  }  

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
        if(this.id) {
          this.isEdit = true;
          this.buttonText = "update book";
          this.booksservice.getBook(this.id, this.userId).subscribe(
            book => {
              this.book_author = book.data().author;
              this.book_name = book.data().title;
            }
          )
    
        }
      }
    )

  }
  
}
