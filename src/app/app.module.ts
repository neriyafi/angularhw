import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';

import { BooksComponent } from './books/books.component';
import { TemperaturesComponent } from './temperatures/temperatures.component';

import { RouterModule, Routes } from '@angular/router';
import { AuthorsComponent } from './authors/authors.component';
import { TempformComponent } from './tempform/tempform.component';
import { EditauthorComponent } from './editauthor/editauthor.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { PostsComponent } from './posts/posts.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { AddbookComponent } from './addbook/addbook.component';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LoginComponent } from './login/login.component';
import { AddpostComponent } from './addpost/addpost.component';



const appRoutes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'posts', component: PostsComponent },
 // { path: 'temperatures', component: TemperaturesComponent},
  { path: 'temperatures/:temp/:city', component: TemperaturesComponent},
  { path: 'tempform', component: TempformComponent},
  { path: 'editauthor/:authname/:id', component: EditauthorComponent},
  //{ path: 'editauthor/:author/:id', component: TemperaturesComponent},
  { path: 'authors/:authname/:id', component: AuthorsComponent },
  { path: 'addbook', component: AddbookComponent },
  { path: 'addbook/:id', component: AddbookComponent },
  { path: 'addpost', component: AddpostComponent },
  { path: 'addpost/:id', component: AddpostComponent },
  { path: 'signup', component: SignUpComponent},
  { path: 'login', component: LoginComponent},
  { path: '',
    redirectTo: '/books',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    NavComponent,
    BooksComponent,
    TemperaturesComponent,
    AuthorsComponent,
    TempformComponent,
    EditauthorComponent,
    PostsComponent,
    AddbookComponent,
    SignUpComponent,
    LoginComponent,
    AddpostComponent,
  ],
  imports: [
    AngularFireAuthModule,
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    MatCardModule, 
    MatFormFieldModule, 
    MatSelectModule,
    MatInputModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),  
  
  AngularFireModule.initializeApp(environment.firebaseConfig),
  AngularFirestoreModule
],
  providers: [AngularFireAuth, AngularFirestore],
  bootstrap: [AppComponent],
 
})
export class AppModule { }
