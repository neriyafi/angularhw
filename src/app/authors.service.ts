import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class AuthorsService {

  authors:any=['Amos oz','J.K.Rolling','Lewis Carrol'];

  getAuthor(){
    const authorsObservable = new Observable(
      observer => {
        setInterval(
        () =>observer.next(this.authors) ,1000
        )
      }
    )
    return authorsObservable;
   }

   addAuthor = (name)=> this.authors.push(name);
   
  constructor() { }
}


