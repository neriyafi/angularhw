import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editauthor',
  templateUrl: './editauthor.component.html',
  styleUrls: ['./editauthor.component.css']
})
export class EditauthorComponent implements OnInit {
authorName:object;
authorid:number;

  constructor(private route: ActivatedRoute,
               private router: Router) { }

  ngOnInit() {
    this.authorName = this.route.snapshot.params.authname;
    this.authorid = this.route.snapshot.params.id;
  }
  onSubmit(){
    this.router.navigate(['/authors',this.authorName,this.authorid]);
  }

}
