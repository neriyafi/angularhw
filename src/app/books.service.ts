import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})


export class BooksService {

 // books:any =  [{id:1 ,title:'Alice in Wonderland', author:'Lewis Carrol'},{id:2 ,title:'War and Peace', author:'Leo Tolstoy'}, {id:3 ,title:'The Magic Mountain', author:'Thomas Mann'}]

  /*getBooks(){
   const booksObservable = new Observable(
     observer => {
       setInterval(
       () =>observer.next(this.books) ,200
       )
     }
   )
   return booksObservable;
  }
*/

userCollection:AngularFirestoreCollection = this.db.collection('users');
bookCollection:AngularFirestoreCollection;

getBooks(userId):Observable<any[]>{
// return this.db.collection('books').valueChanges({idField:'id'});
this.bookCollection = this.db.collection(`users/${userId}/books`);
return this.bookCollection.snapshotChanges().pipe(
  map(
    collection => collection.map(
      document => {
        const data = document.payload.doc.data();
        data.id = document.payload.doc.id;
        return data;
      }
       
    )
  )
)
 }

 getBook(id:string, userId:string):Observable<any>{
  return this.db.doc(`users/${userId}/books/${id}`).get();
  }

 /*
  addBooks(){
    setInterval(
     () => this.books.push({title:'100' , author: 'nadav'}) ,50000000 )
 }  */



 addBooks(userId:string,title:string, author:string){
  const book = {title:title, author:author }
  //this.db.collection('books').add(book)
  this.userCollection.doc(userId).collection('books').add(book);
}

deleteBook(id:string, userId:string){
 this.db.doc(`users/${userId}/books/${id}`).delete();
}

updateBook(userId:string, id:string, title:string, author:string){
  this.db.doc(`users/${userId}/books/${id}`).update( {
    title:title,
    author:author
  })
 }
  constructor(private db:AngularFirestore) { }
}
