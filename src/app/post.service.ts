import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Post} from './interfaces/post';
import {Users} from './interfaces/users';
import { AngularFirestore,AngularFirestoreModule, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private APIPosts = "https://jsonplaceholder.typicode.com/posts/";
  private APIUsers = "https://jsonplaceholder.typicode.com/users/";

    constructor(private http:HttpClient,private db:AngularFirestore)  { }

    
 /* getBlogPost():Observable<Post[]>{
  return this.http.get<Post[]>(`${this.APIPosts}`);
   }
   getUserPost(): Observable<Users[]>{
    return this.http.get<Users[]>(`${this.APIUsers}`);
   } */

   getPosts():Observable<any[]>{
    const ref = this.db.collection('posts');
    return ref.valueChanges({idField: 'id'});
  }

  getPost(id:string):Observable<any>{
    return this.db.doc(`posts/${id}`).get();
  }

  //  addPost(body:String,author:String,title:String){
  //   const posts = {body:body, author:author,title:title}
  //   this.db.collection('posts').add(posts);
  // }
  deletePost(id:string){
    console.log(id)
    this.db.doc(`posts/${id}`).delete();
  }
  
  addPost( title:string, author:string, body:string){
    const post = {title:title, author:author, body:body}
    this.db.collection('posts').add(post);
  }

  updatePost(id:string,title:string, author:string, body:string){
    this.db.doc(`posts/${id}`).update({
      title:title,
      author:author, 
      body:body
    })
  }

}



