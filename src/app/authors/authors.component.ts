import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthorsService } from '../authors.service';
//import { Router } from '@angular/router';
//import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  listsOfAuthor: any;
  listsOfAuthor$:Observable<any>;
  name:any;
constructor(private authorsservice:AuthorsService) { }
 onSubmit(){
            }
ngOnInit() {
  this.listsOfAuthor$=this.authorsservice.getAuthor();
  }
  addAuthors(name){
    this.authorsservice.addAuthor(name);
  }

}
