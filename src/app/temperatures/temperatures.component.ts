import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Weather } from '../interfaces/weather';
import { TempService } from '../temp.service';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {

   
  constructor(private route: ActivatedRoute, private TempService:TempService) { }

  likes = 0;
  temperature; 
  city;
  image:String;
  errorMessege:String;
  hasError:boolean = false;

  $tempData:Observable<Weather>

  addLikes(){
    this.likes++
  }

  ngOnInit() {
   // this.temperature = this.route.snapshot.params.temp;
    this.city= this.route.snapshot.params.city;
    this.$tempData = this.TempService.searchWeatherData(this.city)
    this.$tempData.subscribe(
      data => {
        console.log(data);
        this.temperature = data.temperature;
        this.image = data.image;
      },
      error => {
        this.hasError = true;
        this.errorMessege = error.message;
        console.log('in the component ' + error.message)
      }
    )
  }

}
